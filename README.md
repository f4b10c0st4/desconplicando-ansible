# Treinamento Desconplicando Ansible

Projeto  do treinamento para instalação de um cluster kubernetes utilizando o Ansible + AWS.

## Fases do Projetos
```
 - Provisioning => Criar as instâncias/VMs para o cluster
 - Install_k8s => Criação do cluster, etc.
 - Deploy_app => Deploy de uma aplicação exemplo
 - Extra => Segredo
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
